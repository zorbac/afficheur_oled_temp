# Afficheur OLED
- Lance un serveur bottle avec plusieurs services
  - Service pour positionner ou récupérer temperature et humidité 
  - Page de presentation de l'afficheur OLED
  - Avec un RaspberryPi
    - Lecture de capteur DHT
    - Affichage du contenu sur un ecran Oled connecté en i2c
- Configuration via fichier JSON.

## Pour commencer

### Requis
    Python 3.x
    Freetype (Pillow font ttf)

### Installation
```shell
git clone https://gitlab.com/zorbac/afficheur_oled_temp.git

#Raspberry Pi ou ordinateur developpeur
pip3 install -r pip3-requires.txt

#Complement raspberry Pi
pip3 install -r pip3-requires_rpi.txt
```
Le complement install les modules Adafruit pour la communication OLED ou DHT.

### Configuration

#### Fichier de configuration

Example de fichier de configuration avec t0utes les options disponibles.
```json
{
    "ECRAN": {
        "gpio_pin_rst": 24,
        "i2c_bus": 1,
        "i2c_address": "0x3D"
    },
    "WEB": {
        "port": "8081",
        "service": {
            "retour_home": 1
			"template": "page.tpl"
        }
    },
    "INTERACTION": {
        "font_list": []
    },
    "DHT": [
        {
            "gpio_pin": 17,
            "sensor": 11
        }
    ],
    "EXEMPLE": {
		"TEXTE": [
			"BONJOUR",
			"Tout le monde"
		],
		"TEMPERATURE": [
			{
				"temp": -12.5,
				"humidite": 55.4
			}
		]
	}
}
```
Explications

- ECRAN : [optionnel] écran oled 128x64 connecté en i2c a un RaspberryPi, necessite package Adafruit
  - gpio_pin_rst : E/S utilisé pour reset
  - i2c_bus : le bus utilise dans le système Linux
  - i2c_address : Adresse disponible avec i2cdetect
- WEB : Section du service web disponible
  - port : numero de port d'acces reseau
  - service : option du serveur
    - retour_home 0 aucune page web et seulement reponse json, 1 affichage de la page complète
	- template : nom du fichier template utilisé pour affichage d'information
- INTERACTION : bloc contrôleur principal
  - font_list : liste des polices de caractères a utiliser si listevide utilise tout ceux disponibles
-  DHT [optionnel] liste de capteur connecté au raspberrypi, besoin module Adafruit
  - gpio_pin
  - sensor type de capteur 11, 22, 2301
- EXEMPLE [optionnel] donné de travail pour simuler du contenu
  - TEXTE liste de chaines de caractere a afficher
  - TEMPERATURE liste de dictionnaire contenant la simulation de capteur dht

#### Environnement
- Installer des fichiers de police de caractère (extension ttf) dans le dossier fonts

## Explication lanceurs

- lanceur.py lance les scripts main_ pour avoir un point d'entrée, il prend en parametre
  - demo: Mode démo seulement avec données simulé, ouvrir une page Web et les données seront mises à jour toutes les secondes
  - rpi: Mode de configuration pour le raspberryPi
  - vide: Mode serveur simple accéssible pour verifier le contenu 
- main_lance_rpi_actions.py Ecoute un port GPIO pour executer une action, ici eteindre le RaspberryPi
- main_lance_web.py Lit le fichier de configuration et lance le serveur web
- main_simul_donnees.py: Envoi des données température/humidité et text pour simluer quelque chose sur l'écran OLED
- main_rafraichi_affichage.py Boucle infini de rafraichissement lecture et affichage de valeur. Utilise le fichier de configuration pour connaitre le port du serveur a contacter
- qpython_web_client.py Entete Qpython permettant d'acceder au webkit en mode client.
- termux_affich.sh Script utilise sur termux pour lancer facilement le programme 



## Example utilisation

- Executer la commande
```shell
python3 lanceur.py
```

- Ouvrir un navigateur sur http://127.0.0.1:8081

## Exemples visuels
<img src="images/development.png" alt="Developpement" width="800"> </img>
<img src="images/demo.png" alt="Developpement" width="800"> </img>


## Cablage RaspberryPi
<img src="schema/cablage_rpi_bb.png" alt="Cablage Rpi" width="800"> </img>


## Limites
Pas connues


## Historique versions

* 1 Premiere version

## Meta

Voir ``LICENSE`` pour plus d'information.

## Contribution

Fork du projet
```shell
git clone https://gitlab.com/zorbac/afficheur_oled_temp.git
```
### Dossiers

- Paquets python
  - common Lecture config et preparation logging
  - dht Classe de lecture de capteurs Dht
  - interaction Controleur principal pourle web
  - oled Interraction avec wcran oles et creation image pour oled
  - rpi classes venant d'autre projet permettant de gerer les bputons Rpi
  - web Tous les elements de gestion du serveur web

- Autre dossiers
  - configs fichiers de configuration serveur web et rpi
  - examples fichiers de travail
  - fonts polices de caracteres disponibles 
  - schema Schema Fritzing connexions Rpi
 
# Liens
Liste de liens utiles pour ce script
- https://stackoverflow.com/questions/14348442/how-do-i-display-a-pil-image-object-in-a-template

- Fonts: https://www.dafont.com/bitmap.php?page=2

- URL LIB pour eviter d'utiliser requests
  - https://docs.python.org/3/howto/urllib2.html

- Adapter taille formulaire
  - https://stackoverflow.com/questions/25211090/how-to-auto-adjust-the-div-size-for-all-mobile-tablet-display-formats
  - https://www.webdesignerdepot.com/2013/03/how-to-make-your-contact-forms-mobile-friendly/
  - https://stackoverflow.com/questions/32829567/change-div-order-with-css-depending-on-device-width 
    - http://jsfiddle.net/chdhhm74/
  - 
  
  
  
#Investigations

## RaspberryPi + Python + DHT
* Source : https://github.com/adafruit/Adafruit_Python_DHT

```shell
...Adafruit_Python_DHT/examples
./AdafruitDHT.py 2302 27
./AdafruitDHT.py 11 17
```
Source: https://learn.adafruit.com/dht-humidity-sensing-on-raspberry-pi-with-gdocs-logging/software-install-updated


## Eclipse + Pydev + debogage a distance
Lors mise en place certaines erreurs sont aur le Rpi.
Le debogage distant permet d'investiguer sans se perdre avec des messages logging en plus.

* Pré requis sur la machine distante
```shell
sudo pip3 install pydevd
```
ou
```shell
python3 -m pip install pydevd --user 
```
Test en local sur poste developement
Peu aussi fonctionner sur distant
```python
# breakpoint
#             import sys;sys.path.append(r'/opt/eclipse/plugins/org.python.pydev_6.0.0.201709191431/pysrc')
#             import pydevd;pydevd.settrace('IP_ADRESSE_POSTE_DEV')
```
IP_ADRESSE_POSTE_DEV : Adresse IP du poste de developpement

* Lancer le mode serveur de PyDev disponible dans la perspective debug.
Source : https://sites.google.com/site/programmersnotebook/remote-development-of-python-scripts-on-raspberry-pi-with-eclipse

## Android + termux + mise au point
Termux est un terminal unix dans Android

- A installer dans le système
```shell
packages install python python-dev libjpeg-turbo-dev ndk-sysroot clang freetype freetype-dev libcrypt-dev libzopfli

pip install wheel bottle pillow
```
- alternative si pillow ne s'installe pas
```shell
pip install pillow --global-option="build_ext" --global-option="--disable-jpeg"
```
Non appliqué
$ LDFLAGS="-L/system/lib/" CFLAGS="-I/data/data/com.termux/files/usr/include/" pip install pillow

Source : https://github.com/python-pillow/Pillow/issues/1957


## OLED + i2c

i2cdetect -y 1

## OrangePi + Armbian

- A installer dans le système
```shell
sudo apt-get install python3-pip libfreetype6-dev libfreetype6 libjpeg62-turbo-dev python3-dev
pip3 install wheel --user
```
#TODO

- separe generation temperature avec simulateur
        - ajout sys.path our remonter dans le projet
- utiliser texte pour temp
- deplacer les fichier pour avoir seulement lance a racine
