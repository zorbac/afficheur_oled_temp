'''
@author: zorbac

Copyright 2014 zorbac at free.fr
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import time

if __name__ == '__main__':
    for _ in range(5):
        time.sleep(1)
