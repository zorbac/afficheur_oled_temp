#-*-coding:utf8;-*-
# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import sys
import logging
sys.path.append('/data/data/com.termux/files/usr/lib/python3.6/site-packages/')
try:
    from PIL import Image
    from PIL import ImageDraw
    from PIL import ImageFont
except Exception as e:
    print(e)


class PreparationContenuEcranOled(object):
    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.debug('----------------')
#         self._logger.setLevel(logging.INFO)
#         self._logger.setLevel(logging.DEBUG)

        self._width = 128
        self._heigth = 64

        self._text = list()

    def set_width(self, width):
        self._width = width

    def set_heigth(self, heigth):
        self._heigth = heigth

    def set_fichier_font(self, fichier_font):
        self._fichier_font = fichier_font

    def reset_text(self):
        del self._text[:]

    def add_text(self, p_text):
        '''

        :param p_text:
        '''
        if isinstance(p_text, str):
            self._text.append(p_text)
        elif isinstance(p_text, (list, tuple)):
            self._text = p_text

    def get_image(self):

        # Create blank image for drawing.
        # Make sure to create image with mode '1' for 1-bit color.
        image = Image.new('1', (self._width, self._heigth))

        # Get drawing object to draw on image.
        draw = ImageDraw.Draw(image)

        # Draw a black filled box to clear the image.
        draw.rectangle((0, 0, self._width, self._heigth), outline=0, fill=0)

        if len(self._text) == 0:
            return image

        pos_gauche = 0
        pos_haut = 0

        # Verification si la taille de la police permet l'affichage des
        # lignes

        self._logger.debug(os.path.basename(self._fichier_font))

        limit_hauteur = (self._heigth - 2 - len(self._text)) / len(self._text)
        inter_ligne = 3
        if len(self._text) > 2:
            inter_ligne = 1
        else:
            inter_ligne = 3
        min_hauteur = 10
        if min_hauteur > limit_hauteur:
            limit_hauteur = min_hauteur
        self._logger.debug("limit_hauteur:{}".format(limit_hauteur))

        for _text in self._text:
            font = self._get_font(
                _text, min_hauteur, limit_hauteur, self._fichier_font, self._width)
            # breakpoint remote
#             import sys;sys.path.append(r'/opt/eclipse/plugins/org.python.pydev_6.0.0.201709191431/pysrc')
#             import pydevd;pydevd.settrace('192.168.254.199')

            # Ecriture des ligne de text
            draw.text((pos_gauche, pos_haut), _text,  font=font, fill=255)
            pos_haut = pos_haut + font.getsize(
                _text)[1] + inter_ligne

        return image

    def _get_font(self, p_s_text,  min_hauteur, limit_hauteur, _fichier_font, p_i_limite_longueur=0):
        font_valide = False
        font_size = 12
        _width = p_i_limite_longueur

        # desactivation longueur affichage automatique
        if 0 == p_i_limite_longueur:
            b_long = True
        else:
            b_long = False

        longueur_depasse = 0
        hauteur_depasse = 0
        self._logger.debug(p_s_text)
        last_size = -1

        while not font_valide or last_size != font_size:
            font = ImageFont.truetype(_fichier_font, font_size)
            last_size = font_size

            text_longeur = font.getsize(p_s_text)[0]
            text_hauteur = font.getsize(p_s_text)[1]

            # Definir la taille de la police en fonction de la longueur de
            # l'écran
            if not b_long:
                if text_hauteur >= limit_hauteur:
                    b_long = True
                elif text_longeur > (_width):
                    font_size -= 1
                    longueur_depasse += 1
                    self._logger.debug("-l")
                    b_long = True
                elif text_longeur < (_width) and longueur_depasse == 0:
                    # Gestion cas ou la chaine est vide pour eviter une boucle
                    # infinie avec certaines polices
                    if p_s_text != "":
                        self._logger.debug("+l")
                        font_size += 1
                    else:
                        b_long = True
                else:
                    b_long = True

            if 0 != p_i_limite_longueur and b_long:
                font_valide = True

            # Recherche haiteur optimale
            if b_long:
                if not font_valide:
                    if text_hauteur > limit_hauteur:
                        font_size -= 1
                        hauteur_depasse += 1
                        self._logger.debug("-")
                    # definir au moins hauteur min
                    elif (text_hauteur < min_hauteur or text_hauteur < limit_hauteur)and 0 == hauteur_depasse:
                        font_size += 1
                        self._logger.debug("+")
                    else:
                        if 0 == p_i_limite_longueur:
                            font_valide = True

        self._logger.debug("Final longeur:{}, hauteur:{}".format(
            text_longeur, text_hauteur))
        return font
