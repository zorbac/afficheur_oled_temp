#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import logging


class OledDummy(object):
    def __init__(self):
        self._logger = logging.getLogger()
        self._logger.debug(self.__class__.__name__)
        self._disp = None

    def set_config(self, pin_rst=1, i2c_bus=2, i2c_address=3):
        pass

    def initialize(self):
        pass

    def get_display(self):
        return self._disp

    def display_img(self, image):
        self._logger.debug("Affichage Image")


def lance():
    cmd = OledDummy()
    cmd.display_img(None)


if __name__ == "__main__":
    lance()
