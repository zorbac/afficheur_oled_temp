#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import logging


class OledI2cCommunication(object):
    def __init__(self):
        self._logger = logging.getLogger()
        self._logger.debug(self.__class__.__name__)

        self._disp = None
        self._pin_rst = None
        self._i2c_bus = None
        self._i2c_address = None

    def set_config(self, pin_rst=24, i2c_bus=1, i2c_address=0x3D):
        '''
        
        :param pin_rst: the Pin number as GPIO number
        :param i2c_bus:
        :param i2c_address:
        '''
        # Raspberry Pi pin configuration:
        self._pin_rst = pin_rst
        self._i2c_bus = i2c_bus
        self._i2c_address = i2c_address

    def initialize(self):
        import Adafruit_SSD1306

        # 128x64 display with hardware I2C:
        self._disp = Adafruit_SSD1306.SSD1306_128_64(
            rst=self._pin_rst, i2c_bus=self._i2c_bus, i2c_address=self._i2c_address)

        # Initialize library.
        self._disp.begin()

        # Clear display.
        self._disp.clear()
        self._disp.display()

        #     Pour plus tard
        #width = disp.width
        #height = disp.height

    def get_display(self):
        return self._disp

    def display_img(self, image):

        # Display image.
        self._disp.image(image)
        self._disp.display()


def lance():
    cmd = OledI2cCommunication()


if __name__ == "__main__":
    lance()
