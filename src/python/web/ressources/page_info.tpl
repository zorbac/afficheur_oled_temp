
<!DOCTYPE html>
<html lang="fr">
<head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<!-- sources : 
    http://www.w3schools.com/tags/att_meta_http_equiv.asp 
    http://codesupport.info/6-important-features-as-how-to-do-it-using-meta-tags/
    -->
<meta http-equiv="refresh" content="30">
<meta http-equiv="Cache-Control" content="no-store">
<meta charset="utf-8">
<title>Hugo Page</title>

<meta name="viewport" content="width=device-width, initial-scale=1">


<meta content="Hugo Carnide" name="author">

</head>
<style>

ul.img-list {
  list-style-type: none;
  margin: 0;
  padding: 0;
  text-align: center;
}

ul.img-list li {
  display: inline-block;
  height: 64px;
  margin: 0 1em 1em 0;
  position: relative;
  width: 128px;
}
</style>
<body>

<!--<form action="/mise_a_jour_images" method="post">
    <input type="submit" value="Mise à jour" class="disp" />
</form>-->

<div>
    % for text_line in list_text_affichage:   
        {{text_line}}<br />
    % end
</div>
<span>




<ul class="img-list">



% for image_titre, image_base64_unique in image_base64.items(): 
  <li>
    <a>
        <img alt="{{image_titre}}" src="data:image/png;base64,{{image_base64_unique}}" width="128" height="64"/>
      <span class="text-content"><span>{{image_titre}}</span></span>
    </a>
  </li>
%end
</ul>
</span>
</body>
<footer> </footer>
</html>

