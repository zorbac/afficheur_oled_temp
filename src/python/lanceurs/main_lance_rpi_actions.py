#-*-coding:utf8;-*-
# qpy:3
'''
@author: zorbac

Lanceur principal pour le RaspberryPi pour écouter les appuies bouton pour par exemple l'éteindre

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

from tools import workbench

from rpi import rpi_launcher


if __name__ == "__main__":
    fichier_config, dir_rsrc = workbench.retrieve_inputs()
    workbench.configure(fichier_config, root_dir)

    rpi_launcher.lance()
