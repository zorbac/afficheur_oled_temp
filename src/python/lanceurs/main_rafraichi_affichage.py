#-*-coding:utf8;-*-
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).


import sys
from time import sleep
from urllib.error import URLError
import urllib.request

from common import fonctions_systeme
import lanceurs.main_lance_web as setting
from tools import workbench


TEMPS_ENTRE_RAFRAICHISSEMENT = 600


def lance_rafraichir_page():
    '''
    Rafraichi l'URL du serveur pour mettre à jour l'affichage
    '''
    fichier_config, root_dir = workbench.retrieve_inputs()

    if "" == fichier_config:
        fichier_config = setting.get_config()
   
    if 3 == len(sys.argv):
        temps_attente = float(sys.argv[2])
    else:
        temps_attente   = TEMPS_ENTRE_RAFRAICHISSEMENT
   
    logger, config_json = workbench.configure(fichier_config, root_dir)
   
    port_reseau = "8081"
    if "WEB" in config_json:
        if "port" in config_json["WEB"]:
            port_reseau = config_json["WEB"]["port"]

    ip_address = fonctions_systeme.get_adresse_ip()

    url_update = "http://{}:{}/mise_a_jour_images".format(
        ip_address, port_reseau)

    # Ajout valeurs pour être sûr d'utiliser un POST
    values = {}
    data = urllib.parse.urlencode(values)
    data = data.encode('ascii')  # data should be bytes

    boucle_infinie = True
    while boucle_infinie:

        try:
            with urllib.request.urlopen(url_update, data) as response:
                html = response.read()
                if "error" in str(html).lower():
                    boucle_infinie = False
                # Alternative pour avoir la réponse json
                #         result = json.loads(response.readall().decode('utf-8'))
                sleep(temps_attente)
        except URLError as e:
            boucle_infinie = False
            logger.error("Exception", e)
    logger.exception("URL rafraichissement à renvoyé une erreur")


def fonctions_tests():

    #     url = "http://{}:8081".format(ip_address)
    #     with urllib.request.urlopen(url) as response:
    #         html = response.read()
    #     print(html)

    #     import requests
    #     response = requests.post(url_update)
    #     print(response.text)
    pass


if __name__ == '__main__':
    lance_rafraichir_page()
