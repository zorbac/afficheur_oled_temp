#-*-coding:utf8;-*-
'''
@author: zorbac

'''

# license
#
# Producer 2017 zorbac at free.fr
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

from _io import BytesIO
import base64

from oled.preparation_contenu_ecran_oled import PreparationContenuEcranOled
import logging
import os
from collections import OrderedDict


class Controler(object):
    '''
    '''
    TEMP_HUM_FORMAT = "{:0.1f}° {:0.1f}%"
#    TEMP_HUM_FORMAT = "{:0.1f}c {:0.1f}p"
    MAX_TEXT = 4

    def __init__(self):
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.debug('----------------')
        self._config = None
        self._list_dict_temp = list()
        self._list_text = list()
        self._list_displayed_text = list()
        self._max_list_text = 4
        self._max_list_temp = 4
        self._display = None
        self._list_dht_lecteur = None
        self._list_font = None
        self._dict_image = dict()
        self._img_last_index = -1
        self._list_images = list()
        self._mise_a_jour = False
        self._mise_a_jour_dict = False
        self._dict_imag_base64 = None

    @staticmethod
    def get_interraction(configuration, list_font=None):
        interact = Controler()
        interact.set_configuration(configuration, list_font)
        interact.configure()
        return interact

    def set_configuration(self, configuration, list_font=None):
        self._config = configuration
        height = 64
        self._ecran = PreparationContenuEcranOled()
        self._ecran.set_heigth(height)
        self._ecran.set_width(128)

        if list_font is not None:
            if "font_list" in configuration:
                list_selection_font = configuration["font_list"]
                if len(list_selection_font) > 0:
                    self._list_font = list()
                    for font_selection in list_selection_font:
                        for font in list_font:
                            if font_selection.strip() in font:
                                self._list_font.append(font)
            if self._list_font is None:
                self._list_font = list_font
            self._list_font.sort()

    def configure(self):
        pass

    def get_valeurs_temp(self, index=None):
        if len(self._list_dict_temp) > 0:
            if index is not None:
                return [self._list_dict_temp[index]]
            else:
                return self._list_dict_temp
        return None

    def get_nombre_temp(self):
        if self._list_dht_lecteur is not None:
            return len(self._list_dht_lecteur)
        else:
            return len(self._list_dict_temp)

    def lit_valeurs(self, index=None):
        if self._list_dht_lecteur is not None:
            if index is not None:
                if index < len(self._list_dht_lecteur):
                    val_lues = self._list_dht_lecteur[index].obtenir_infos()
                    self.add_valeurs_temp(val_lues[0], val_lues[1], index)
                else:
                    self._logger.exception("Index hors limites")
            else:
                for idx, dht in enumerate(self._list_dht_lecteur):
                    val_lues = dht.obtenir_infos()
                    self.add_valeurs_temp(val_lues[0], val_lues[1], idx)

    def add_valeurs_temp(self, temperature, humidite, index=None):

        dict_val = {'temp': float(temperature), 'humidity': float(humidite)}
        self._logger.debug('{},{}'.format(dict_val, index))

        if index is not None:
            if index + 1 > len(self._list_dict_temp):
                for _ in range(len(self._list_dict_temp), index + 1):
                    self._list_dict_temp.append(None)
            self._list_dict_temp[index] = dict_val
            text_idx = index
        else:
            if len(self._list_dict_temp) == self._max_list_temp:
                self._list_dict_temp.pop(0)
            self._list_dict_temp.append(dict_val)
            text_idx = len(self._list_dict_temp) - 1
        self._mise_a_jour = True
        return text_idx

    def reset_valeurs_temp(self):
        del self._list_dict_temp[:]
        self._mise_a_jour = True

    def get_text(self, index=None):
        if index is not None:
            return [self._list_text[index]]
        else:
            return self._list_text

    def add_text(self, p_text, p_index=None):
        '''
        Ajoute le texte et si trop de texte enlève le plus ancien.
        :param p_text:
        :param p_index:
        '''
        self._logger.debug("params {}, {}".format(p_text, p_index))
        if p_index is not None:
            if p_index == len(self._list_text):
                for _ in range(len(self._list_text), p_index + 1):
                    self._list_text.append("-")
            self._list_text[p_index] = p_text
            text_idx = p_index
        else:
            # Suppression de la premiere chaine de caractere pour faire
            # apparaitre les suivantes
            if len(self._list_text) == self._max_list_text:
                self._list_text.pop(0)
            self._list_text.append(p_text)
            text_idx = len(self._list_dict_temp) - 1
        self._mise_a_jour = True

        return text_idx

    def reset_text(self):
        del self._list_text[:]
        self._mise_a_jour = True

    def _update_displayed_text(self):
        del self._list_displayed_text[:]
        for info in self._list_dict_temp:
            text = self.TEMP_HUM_FORMAT.format(info['temp'], info['humidity'])

            self._list_displayed_text.append(text)

        for text_man in self._list_text:
            if self.MAX_TEXT > len(self._list_displayed_text):
                self._list_displayed_text.append(text_man)

    def get_displayed_text(self, index=None):
        self._update_displayed_text()
        if index is not None:
            return [self._list_displayed_text[index]]
        else:
            return self._list_displayed_text

    def _creation_images(self):
        '''
        Creation dictionnaire avec des images
        '''
        if self._mise_a_jour == True:

            # TODO détecter changement pour mettre à jour images
            self._update_displayed_text()
            del self._list_images[:]
            for font in self._list_font:
                base_font = os.path.basename(font)

                self._ecran.set_fichier_font(font)
                self._ecran.add_text(self._list_displayed_text)
                imag = self._ecran.get_image()

                dict_cur = dict()
                dict_cur[base_font] = imag
                # Utilisation list pour savoir quel image est affiché sur l'écran
                # OLED, lourd et gourmand en mémoire à revoir
                self._list_images.append(dict_cur)

                self._dict_image[base_font] = imag
            self._mise_a_jour_dict = True
            self._mise_a_jour = False

    def _convertion_images_base64(self):
        '''
        Conversion en base64 pour integration page web

        :param dict_image:
        '''
        if self._dict_imag_base64 is None:
            self._mise_a_jour_dict = True
        if self._mise_a_jour_dict:
            if self._dict_imag_base64 is None:
                self._dict_imag_base64 = dict()
            else:
                self._dict_imag_base64.clear()
            for title, imag in self._dict_image.items():
                buffer = BytesIO()
                imag.save(buffer, format="PNG")
                img_str = base64.b64encode(buffer.getvalue())
                strg = str(img_str)

                # Ajout image dans la liste
                self._dict_imag_base64[title] = strg[2:-1]
            self._mise_a_jour_dict = False

    def set_oled(self, oled_display):
        self._display = oled_display

    def set_dht(self, list_dht_lecteur):
        self._list_dht_lecteur = list_dht_lecteur

    def display_text(self):
        if self._display is not None:
            self._creation_images()
            self._img_last_index = (
                self._img_last_index + 1) % len(self._list_images)
            self._logger.debug("Index image: %i", self._img_last_index)
            self._display.display_img(self._list_images[self._img_last_index])

    def get_images(self):
        if len(self._dict_image) == 0:
            self._mise_a_jour = True
            self._creation_images()
        return self._dict_image

    def get_images_base64(self):
        self.get_images()
        self._convertion_images_base64()
        return self._dict_imag_base64


if __name__ == '__main__':
    config = ['t3st']
    interaction = Controler.get_interraction(config)
